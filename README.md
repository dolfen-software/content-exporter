# content-exporter

A simple prometheus exporter that allows monitoring of particular strings on a website (e.g. script snippets or headlines). WIP.

# Running this software

This is designed to be used as a docker image hosted on your prometheus server, like the [blackbox_exporter](https://github.com/prometheus/blackbox_exporter).
The docker image is available at: https://hub.docker.com/r/tdolfen/content_exporter

## Configuration

Docker volumes need to be mounted at three locations, the last one being optional: 
- /content_exporter/config/targets
- /content_exporter/check_snippets
- /content_exporter/config

First, you'll need to configure your targets with a [targets.yaml file](https://gitlab.com/dolfen-software/content-exporter/-/blob/main/app/config/targets/targets.yaml). 
Specifying the names of the snippets for each target, you'll need to create a .txt file in [check_snippets](https://gitlab.com/dolfen-software/content-exporter/-/tree/main/app/check_snippets/example-project-1.txt).
Lastly, if you feel the need to overwrite some general settings for this exporter (see [Exporter Options](#exporter-options)), you will need a config.yaml which will be mounted in `/content_exporter/config`.

## Checking the results

Visiting http://localhost:12911/metrics will return metrics just in time for your configured targets.
