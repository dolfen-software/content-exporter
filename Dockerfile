FROM golang:1.18.4-alpine3.16

WORKDIR /content_exporter

COPY ./app/check_snippets ./check_snippets
COPY ./app/config/ ./config
COPY ./app/go.mod ./
COPY ./app/go.sum ./

RUN go mod download

COPY ./app/main.go ./

RUN go build -o /content-exporter

EXPOSE 12911

ENTRYPOINT [ "/content-exporter" ]