package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"reflect"
	"strings"
	"unicode"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/visionmedia/go-cli-log"
	"gopkg.in/yaml.v2"
)

type (
	Config struct {
		Targets []struct {
			URL     string `yaml:"url"`
			Check   string `yaml:"check"`
			Project string `yaml:"project"`
			Cookies []struct {
				Name   string `yaml:"name"`
				Value  string `yaml:"value"`
				Domain string `yaml:"domain,omitempty"`
				Path   string `yaml:"path,omitempty"`
			} `yaml:"cookies,omitempty"`
			Auth []struct {
				Type  string `yaml:"type"`
				Name  string `yaml:"name,omitempty"`
				Pass  string `yaml:"pass,omitempty"`
				Value string `yaml:"value,omitempty"`
			} `yaml:"auth,omitempty"`
		} `yaml:"targets"`
	}

	ExporterOptions struct {
		LogLevel    int    `yaml:"logLevel"`
		MetricsPath string `yaml:"metricsPath"`
	}

	Cookie struct {
		Name   string
		Value  string
		Domain string
		Path   string
	}

	CookieRecord struct {
		Cookies []Cookie
	}

	Authorization struct {
		Type  string
		Name  string
		Pass  string
		Value string
	}
)

var (
	logLevel        = 4
	metricsPath     = "/metrics"
	registry        = prometheus.NewRegistry()
	targets         Config
	exporterOptions ExporterOptions
)

func loadGlobalConfigAndSetOptions() {
	configFile, _ := filepath.Abs("./config/config.yaml")
	configYaml, err := ioutil.ReadFile(configFile)

	if err != nil {
		if logLevel > 0 {
			log.Error(err)
		}
		panic(err)
	}

	err = yaml.Unmarshal(configYaml, &exporterOptions)

	if err != nil {
		if logLevel > 0 {
			log.Error(err)
		}
		panic(err)
	}

	logLevelType := reflect.TypeOf(exporterOptions.LogLevel).Kind()
	metricsPathType := reflect.TypeOf(exporterOptions.MetricsPath).Kind()

	if logLevelType == reflect.Int {
		logLevel = exporterOptions.LogLevel
	}
	if metricsPathType == reflect.String {
		metricsPath = exporterOptions.MetricsPath
	}
	if logLevel > 2 {
		log.Info("Info", "Loaded exporter config file")
	}
}

func loadTargetsConfig() {
	filename, _ := filepath.Abs("./config/targets/targets.yaml")
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		if logLevel > 0 {
			log.Error(err)
		}
		panic(err)
	}

	err = yaml.Unmarshal(yamlFile, &targets)

	if err != nil {
		if logLevel > 0 {
			log.Error(err)
		}
		panic(err)
	}
}

func setMetricPoints() {
	for _, targets := range targets.Targets {
		filePath := "./check_snippets/" + targets.Check
		filename, _ := filepath.Abs(filePath)
		checkFile, err := ioutil.ReadFile(filename)

		if err != nil {
			if logLevel > 0 {
				log.Error(err)
			}
			panic(err)
		}

		checkContents := SpaceMap(string(checkFile))
		url := targets.URL
		project := targets.Project
		cookieLabel := "no_cookies"
		var cookies CookieRecord
		var authorizaton Authorization

		if len(targets.Cookies) > 0 {
			cookieLabel = fmt.Sprintf("%d %s", len(targets.Cookies), "cookies")
			for _, cookie := range targets.Cookies {
				if cookie.Domain == "" {
					cookie.Domain = url
				}
				if cookie.Path == "" {
					cookie.Path = "/"
				}
				cookies.Cookies = append(cookies.Cookies, Cookie{cookie.Name, cookie.Value, cookie.Domain, cookie.Path})
			}
		}

		authUsed := false

		if len(targets.Auth) > 0 {
			authUsed = true
			for _, auth := range targets.Auth {
				switch auth.Type {
				case "basic":
					authorizaton = Authorization{auth.Type, auth.Name, auth.Pass, ""}
				case "bearer":
					authorizaton = Authorization{auth.Type, "", "", auth.Value}
				default:
					authUsed = false
					authorizaton = Authorization{"", "", "", ""}
				}
			}
		}

		if err := registry.Register(prometheus.NewGaugeFunc(
			prometheus.GaugeOpts{
				Namespace:   "content_exporter",
				Name:        "check_for_snippets",
				Help:        "Check if snippet exists in site.",
				ConstLabels: prometheus.Labels{"url": url, "project": project, "cookies": cookieLabel, "authUsed": fmt.Sprintf("%t", authUsed)},
			},
			func() float64 {
				webpageContent := SpaceMap(OnPage(url, cookies, authorizaton))
				containsOrNot := strings.Contains(webpageContent, checkContents)

				if containsOrNot {
					if logLevel > 3 {
						log.Debug("Debug", "Scrape successful, URL %s (Project %s) returned %t", url, project, containsOrNot)
					}
					return float64(1)
				} else {
					if logLevel > 0 {
						log.Warn("Scrape returned false for URL %s", url)
					}
					return float64(0)
				}
			},
		)); err == nil {
			if logLevel > 3 {
				log.Debug("Debug", "GaugeFunc 'content_exporter' for project %s and URL %s registered successfully", targets.Project, targets.URL)
			}
		} else {
			if logLevel > 0 {
				log.Error(err)
			}
		}
	}
}

func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}

func OnPage(link string, cookies CookieRecord, authorizaton Authorization) string {
	// Make a new request with cookies
	req, err := http.NewRequest("GET", link, nil)

	if err != nil {
		log.Error(err)
	}

	for _, cookie := range cookies.Cookies {
		cookie := &http.Cookie{
			Name:   cookie.Name,
			Value:  cookie.Value,
			Domain: cookie.Domain,
			Path:   cookie.Path,
		}
		req.AddCookie(cookie)
	}

	switch authorizaton.Type {
	case "basic":
		req.Header.Add("Authorization", "Basic "+basicAuth(authorizaton.Name, authorizaton.Pass))
	case "bearer":
		req.Header.Set("Authorization", "Bearer "+authorizaton.Value)
	default: // No auth
	}

	log.Info("Info", "Request headers: %s", req.Header)

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Error(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Error(err)
	}

	return string(body)
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	// parse global config options for this exporter
	// available:
	// log levels for stdout
	// metrics path
	// port to listen
	log.Verbose = true

	loadGlobalConfigAndSetOptions()

	/*
		Error: 4		 >0
		Warning: 3		 >1
		Info: 2			 >2
		Debug: 1		 >3
	*/
	if logLevel > 3 {
		log.Debug("Debug", "Parsing targets.yaml.")
	}

	loadTargetsConfig()

	if logLevel > 2 {
		log.Info("Info", "Creating metric points for %d targets", len(targets.Targets))
	}

	setMetricPoints()

	if logLevel > 2 {
		log.Info("Info", "All metric points registered successfully.")
		log.Info("Info", "Setting up http handlers.")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if logLevel > 3 {
			log.Debug("Debug", "Received request for /")
		}

		w.Write([]byte(`<html>
			<head><title>content-exporter</title></head>
			<body>
			<h1>content-exporter</h1>
			<p><a href="/metrics">Metrics</a></p>
			</body>
			</html>`))
	})
	handler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
	http.Handle(metricsPath, handler)
	if logLevel > 2 {
		log.Info("Info", "Server ready to accept requests.")
	}
	http.ListenAndServe(":12911", nil)
}
